
import Text.Printf

romSize :: Int
romSize = 32768

zero :: Int
zero = 0

main :: IO ()
main = do
  let zeros = replicate romSize zero in
    mapM_ (\x -> printf "%c" x) zeros
